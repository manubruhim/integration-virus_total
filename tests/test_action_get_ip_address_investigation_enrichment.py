import mock
import responses
import json
from virus_total_base_test import VirusTotalBaseTest
from custom_core.virus_total_client import VirusTotalClient
from _GEN_get_ip_address_investigation_enrichment import \
    GetIpAddressInvestigationEnrichment


class IpInvestigationEnrichmentTestCase(VirusTotalBaseTest):
    __test__ = True
    action_cls = GetIpAddressInvestigationEnrichment
    invalid_ip = "0.0.0"
    valid_ip = "0.0.0.0"
    list_of_relationships = ['comments', 'downloaded_files',
                             'related_comments', 'referrer_files',
                             'resolutions', 'urls']
    invalid_relationship = "invalid-relationship"

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrichment_invalid_ip_and_no_relationships(self, client):
        client.return_value = VirusTotalClient(**self.config)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.invalid_ip)
        self.assertTrue(status)
        self.assertEquals(value, {"error_message":
                                  "Given IP is not valid"})

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrichment_valid_ip_and_invalid_relationships(self, client):
        client.return_value = VirusTotalClient(**self.config)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.valid_ip,
            relationships=self.invalid_relationship)

        self.invalid_relationship = '"' + self.invalid_relationship + '"'
        expected = f"{self.invalid_relationship} is not one of " \
                   f"allowed categories ['comments', " \
                   f"'downloaded_files', 'related_comments', " \
                   f"'referrer_files', 'resolutions', " \
                   f"'urls']<br> Please provide allowed " \
                   f"category or check for typos"
        self.assertIsNotNone(value)
        self.assertIn('error_message', value)
        self.assertTrue(status)
        self.assertIn(expected, list(value.values())[0])

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrichment_valid_ip_and_comments_relation(self, client):
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(self.get_fixture_content(
            "get_ip_address_investigation_"
            "enrichment_comments_result.json"))
        responses.add(responses.GET,
                      "https://www.test.com/api/v3/ip_addresses/"
                      "{}?relationships={}".
                      format(self.valid_ip,
                             self.list_of_relationships[0]),
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.valid_ip,
            relationships=self.list_of_relationships[0])

        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrich_valid_ip_and_downloaded_files_relation(self, client):
        self.maxDiff = None
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(self.get_fixture_content(
            "get_ip_address_investigation_enrichment_"
            "downloaded_files_result.json"))
        responses.add(responses.GET,
                      "https://www.test.com/api/v3/ip_addresses/"
                      "{}?relationships={}".
                      format(self.valid_ip,
                             self.list_of_relationships[1]),
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.valid_ip,
            relationships=self.list_of_relationships[1])

        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrich_valid_ip_and_related_comments_relation(self, client):
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(self.get_fixture_content(
            "get_ip_address_investigation_enrichment_"
            "related_comments_result.json"))

        responses.add(responses.GET,
                      "https://www.test.com/api/v3/ip_addresses/"
                      "{}?relationships={}".
                      format(self.valid_ip,
                             self.list_of_relationships[2]),
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.valid_ip,
            relationships=self.list_of_relationships[2])
        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrich_valid_ip_and_referrer_files_relation(self, client):
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(self.get_fixture_content(
            "get_ip_address_investigation_enrichment_"
            "referrer_files_result.json"))

        responses.add(responses.GET,
                      "https://www.test.com/api/v3/ip_addresses/"
                      "{}?relationships={}".
                      format(self.valid_ip,
                             self.list_of_relationships[3]),
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.valid_ip,
            relationships=self.list_of_relationships[3])
        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_valid_ip_and_valid_resolutions_relationships(self, client):
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(self.get_fixture_content(
            "get_ip_address_investigation_"
            "enrichment_resolutions_result.json"))

        responses.add(responses.GET,
                      "https://www.test.com/api/v3/ip_addresses/"
                      "{}?relationships={}".
                      format(self.valid_ip,
                             self.list_of_relationships[4]),
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.valid_ip,
            relationships=self.list_of_relationships[4])
        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrich_valid_ip_and_valid_urls_relation(self, client):
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(self.get_fixture_content(
            "get_ip_address_investigation_enrichment_urls_result.json"))

        responses.add(responses.GET,
                      "https://www.test.com/api/v3/ip_addresses/"
                      "{}?relationships={}".
                      format(self.valid_ip,
                             self.list_of_relationships[5]),
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.valid_ip,
            relationships=self.list_of_relationships[5])
        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrich_valid_ip_and_all_valid_relationships(self, client):
        self.relationship = "comments,downloaded_files," \
                            "related_comments," \
                            "referrer_files,resolutions,urls"
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(self.get_fixture_content(
            "get_ip_address_investigation_enrichment_all_result.json"))

        responses.add(responses.GET,
                      "https://www.test.com/api/v3/ip_addresses/"
                      "{}?relationships={}".
                      format(self.valid_ip,
                             self.relationship),
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            ip=self.valid_ip)
        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)
