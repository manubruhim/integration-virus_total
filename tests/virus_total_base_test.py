from st2tests.base import BaseActionTestCase


class VirusTotalBaseTest(BaseActionTestCase):
    __test__ = False

    def setUp(self):
        super(VirusTotalBaseTest, self).setUp()
        self.instance_name = 'api_virus_total'
        self.url = "https://www.test.com/api/v3/search"
        self.config = {'base_url': 'https://www.test.com/api/v3',
                       'api_key': ''}
