import mock
import responses
import json

from virus_total_base_test import VirusTotalBaseTest
from custom_core.virus_total_client import VirusTotalClient
from _GEN_enrich_file_hash import EnrichFileHash


class EnrichFileHashTestCase(VirusTotalBaseTest):
    __test__ = True
    action_cls = EnrichFileHash
    file_hash = "8B2E701E91101955C73865589A4C72999AE" \
                "ABC11043F712E05FDB1C17C4AB19A"

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrich_file_hash_not_valid(self, client):
        client.return_value = VirusTotalClient(**self.config)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            file_hash=self.file_hash[:10])
        self.assertTrue(status)
        self.assertEquals(value, {"error_message":
                                  "Given File Hash is not valid"})

    @responses.activate
    @mock.patch("custom_core.virus_total_actions."
                "VirusTotalActions.get_client")
    def test_enrich_file_hash(self, client):
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(
            self.get_fixture_content("enrich_file_hash_result.json"))
        responses.add(responses.GET, self.url,
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            file_hash=self.file_hash)
        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)
