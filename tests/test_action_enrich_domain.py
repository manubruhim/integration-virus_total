import mock
import responses
import json

from virus_total_base_test import VirusTotalBaseTest
from custom_core.virus_total_client import VirusTotalClient
from _GEN_enrich_domain import EnrichDomain


class EnrichDomainTestCase(VirusTotalBaseTest):
    __test__ = True
    action_cls = EnrichDomain
    test_domain = "facebook.com"

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrich_url_not_valid(self, client):
        client.return_value = VirusTotalClient(**self.config)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            domain="facebook.d")
        self.assertTrue(status)
        self.assertEquals(value, {"error_message":
                                  "Given Domain is not valid"})

    @responses.activate
    @mock.patch("custom_core.virus_total_actions."
                "VirusTotalActions.get_client")
    def test_enrich_domain(self, client):
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(
            self.get_fixture_content("enrich_domain_result.json"))

        responses.add(responses.GET,
                      self.url,
                      status=200,
                      json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            domain=self.test_domain)

        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)
