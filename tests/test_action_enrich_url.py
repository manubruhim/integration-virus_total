import mock
import responses
import json

from virus_total_base_test import VirusTotalBaseTest
from custom_core.virus_total_client import VirusTotalClient
from _GEN_enrich_url import EnrichUrl


class EnrichUrlTestCase(VirusTotalBaseTest):
    __test__ = True
    action_cls = EnrichUrl
    test_url = "https://testsafebrowsing.appspot.com/s/phishing.html"

    @responses.activate
    @mock.patch('custom_core.virus_total_actions.'
                'VirusTotalActions.get_client')
    def test_enrich_url_not_valid(self, client):
        client.return_value = VirusTotalClient(**self.config)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            url="http://google/3628126748")
        self.assertTrue(status)
        self.assertEquals(value,
                          {"error_message":
                           "Given URL is not valid"})

    @responses.activate
    @mock.patch("custom_core.virus_total_actions."
                "VirusTotalActions.get_client")
    def test_enrich_url(self, client):
        client.return_value = VirusTotalClient(**self.config)
        expected = json.loads(
            self.get_fixture_content("enrich_url_result.json"))

        responses.add(responses.GET, self.url, status=200, json=expected)
        status, value = self.get_action_instance().run(
            instance_name=self.instance_name,
            url=self.test_url)

        self.assertIsNotNone(value)
        self.assertNotIn('error_message', value)
        self.assertEqual(expected, value)
        self.assertTrue(status)
