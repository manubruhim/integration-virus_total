import pytest
from tests_utils.loggers import auto_steps
from tests.stackstorm.base_workflow_test import BaseWorkflowTestCase
from stackstorm_tests_utils.api_clients_collections.cdc_clients import FrontendApiClient


@auto_steps
class TestWorkflows(BaseWorkflowTestCase):
    """
    This test suite is for all test related to workflows of the virus_total functionality.

    - Task : `<https://bi-sec.atlassian.net/browse/UCA-4342>`_
    """

    @pytest.mark.stackstorm
    @pytest.mark.virus_total_workflows
    @pytest.mark.parametrize("ip", [("8.8.8.8"), ("175.193.126.46"), ("127.0.0.1")])
    def test_enrich_ip_cli(self, admin_api: FrontendApiClient, request, ip: str) -> None:
        r"""
        **Test Name: Enrich ip cli**

            * **Author:** `Nathan <mailto:natan.melamed@cyberproof.com>`_

            * **Tags:** #St2Tests, #WorkflowTests, #VirusTotal

        - **Test description:** Run "enrich_ip_cli" cli command in CDC about a certain ip address.

        - **Test Prerequisites:** "virus_total" & "cdc_sdk" packs installed,
                                  datastore keys configured, user is logged into St2 and CDC env

        - **Test Steps:**
            1. Inject some alert to CDC using the following POST request
               'https://{cdc_url}/app/alert' with
            2. In the left-side navigation bar click on the "Alerts" button
            3. Search the alert entity injected in the alerts grid
            4. In the message field run the following cli command
               '/virus_total enrich_ip_cli  --ip={ip_address}'

        - **Expected Result:**
            1. The results of enrich_ip_cli command should be injected as thread in CDC under
               the cli message
            2. A result data should be correspond to the following endpoint of the mock:
               `https://{mock_url}/virus_total/api/v3/check_result`
               with "query" query param {ip}


        - **Epic/Task Link:**
            `Implement automation tests for VirusTotal workflows
            <https://bi-sec.atlassian.net/browse/UCA-4342>`_

        - **Run Command:**
            `pytest --env={ENV-TEST} tests/stackstorm/virus_total/workflows
            -k test_enrich_ip_cli --alluredir=results`

        """
        mock_response: dict = self.installed_packs[1]["mock_client"].search_result(ip)
        actual_response: dict = self.create_cli_message(cdc_client=admin_api, request=request,
                                                        ip=ip)
        assert mock_response == actual_response

    @pytest.mark.stackstorm
    @pytest.mark.virus_total_workflows
    @pytest.mark.parametrize("domain", [("abuse.net"), ("google.com")])
    def test_enrich_domain_cli(self, admin_api: FrontendApiClient, request, domain: str) -> None:
        r"""
        **Test Name: Enrich domain cli**

            * **Author:** `Nathan <mailto:natan.melamed@cyberproof.com>`_

            * **Tags:** #St2Tests, #WorkflowTests, #VirusTotal

        - **Test description:** Run "enrich_domain_cli" cli command in CDC about a certain
                                domain address.

        - **Test Prerequisites:** "virus_total" & "cdc_sdk" packs installed,
                                  datastore keys configured, user is logged into St2 and CDC env

        - **Test Steps:**
            1. Inject some alert to CDC using the following POST request
               'https://{cdc_url}/app/alert' with
            2. In the left-side navigation bar click on the "Alerts" button
            3. Search the alert entity injected in the alerts grid
            4. In the message field run the following cli command
               '/virus_total enrich_domain_cli  --domain={domain_address}'

        - **Expected Result:**
            1. The results of enrich_domain_cli command should be injected as thread in CDC under
               the cli message
            2. A result data should be correspond to the following endpoint of the mock:
               `https://{mock_url}/virus_total/api/v3/check_result`
               with "query" query param {domain}


        - **Epic/Task Link:**
            `Implement automation tests for VirusTotal workflows
            <https://bi-sec.atlassian.net/browse/UCA-4342>`_

        - **Run Command:**
            `pytest --env={ENV-TEST} tests/stackstorm/virus_total/workflows
            -k test_enrich_domain_cli --alluredir=results`

        """
        mock_response: dict = self.installed_packs[1]["mock_client"].search_result(domain)
        actual_response: dict = self.create_cli_message(cdc_client=admin_api, request=request,
                                                        domain=domain)
        assert mock_response == actual_response

    @pytest.mark.stackstorm
    @pytest.mark.virus_total_workflows
    @pytest.mark.parametrize("url", [("https://bitbucket.org")])
    def test_enrich_url_cli(self, admin_api: FrontendApiClient, request, url: str) -> None:
        r"""
        **Test Name: Enrich url cli**

            * **Author:** `Nathan <mailto:natan.melamed@cyberproof.com>`_

            * **Tags:** #St2Tests, #WorkflowTests, #VirusTotal

        - **Test description:** Run "enrich_url_cli" cli command in CDC about a certain
                                url address.

        - **Test Prerequisites:** "virus_total" & "cdc_sdk" packs installed,
                                  datastore keys configured, user is logged into St2 and CDC env

        - **Test Steps:**
            1. Inject some alert to CDC using the following POST request
               'https://{cdc_url}/app/alert' with
            2. In the left-side navigation bar click on the "Alerts" button
            3. Search the alert entity injected in the alerts grid
            4. In the message field run the following cli command
               '/virus_total enrich_url_cli  --url={url_address}'

        - **Expected Result:**
            1. The results of enrich_url_cli command should be injected as thread in CDC under
               the cli message
            2. A result data should be correspond to the following endpoint of the mock:
               `https://{mock_url}/virus_total/api/v3/check_result`
               with "query" query param {url}


        - **Epic/Task Link:**
            `Implement automation tests for VirusTotal workflows
            <https://bi-sec.atlassian.net/browse/UCA-4342>`_

        - **Run Command:**
            `pytest --env={ENV-TEST} tests/stackstorm/virus_total/workflows
            -k test_enrich_url_cli --alluredir=results`

        """
        mock_response: dict = self.installed_packs[1]["mock_client"].search_result(url)
        actual_response: dict = self.create_cli_message(cdc_client=admin_api, request=request,
                                                        url=url)
        assert mock_response == actual_response

    @pytest.mark.stackstorm
    @pytest.mark.virus_total_workflows
    @pytest.mark.parametrize("relationships",
                             [(""),
                              ("comments"),
                              ("comments,downloaded_files"),
                              ("comments,downloaded_files,related_comments"),
                              ("related_comments,referrer_files,comments"),
                              ("comments,downloaded_files,urls"),
                              ("comments,downloaded_files,related_comments,"
                               "referrer_files,resolutions,urls")])
    @pytest.mark.parametrize("ip", [("1.1.1.1"), ("8.8.8.8"), ("79.238.22.30"), ("223.83.216.125")])
    def test_get_ip_address_investigation_enrichment_cli(self, admin_api: FrontendApiClient,
                                                         request, ip: str,
                                                         relationships: str) -> None:
        r"""
        **Test Name: Get ip address investigation enrichment cli**

            * **Author:** `Nathan <mailto:natan.melamed@cyberproof.com>`_

            * **Tags:** #St2Tests, #WorkflowTests, #VirusTotal

        - **Test description:** Run "get_ip_address_investigation_enrichment_cli" cli command
                                in CDC about a certain ip address.

        - **Test Prerequisites:** "virus_total" & "cdc_sdk" packs installed,
                                  datastore keys configured, user is logged into St2 and CDC env

        - **Test Steps:**
            1. Inject some alert to CDC using the following POST request
               'https://{cdc_url}/app/alert' with
            2. In the left-side navigation bar click on the "Alerts" button
            3. Search the alert entity injected in the alerts grid
            4. In the message field run the following cli command
               '/virus_total get_ip_address_investigation_enrichment_cli
               --ip={ip_address}  --relationships={relationships}'

        - **Expected Result:**
            1. The results of get_ip_address_investigation_enrichment_cli
               command should be injected as thread in CDC under the cli message
            2. A result data should be correspond to the following endpoint of the mock:
               `https://{mock_url}/virus_total/api/v3/get_investigation_enrichment_result`
               with "ip" query param {ip} and "relationships" param {relationships}


        - **Epic/Task Link:**
            `Implement automation tests for VirusTotal workflows
            <https://bi-sec.atlassian.net/browse/UCA-4342>`_

        - **Run Command:**
            `pytest --env={ENV-TEST} tests/stackstorm/virus_total/workflows
            -k test_get_ip_address_investigation_enrichment_cli --alluredir=results`

        """
        mock_response: dict = self.installed_packs[1][
            "mock_client"].get_investigation_enrichment_result(ip=ip, relationships=relationships)
        if relationships == "":
            actual_response: dict = self.create_cli_message(cdc_client=admin_api, request=request,
                                                            ip=ip)
        else:
            actual_response: dict = self.create_cli_message(cdc_client=admin_api,
                                                            request=request,
                                                            ip=ip, relationships=relationships)
        assert mock_response == actual_response
