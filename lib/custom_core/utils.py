from general.datetime_parser import parse
from st2_utils import generic_functions
from custom_core.relationship_enum import RelationshipEnum

TIME_FORMAT = "%b %d, %Y at %H:%M:%S %p %Z"


def key_replace(key: str) -> str:
    return key.replace(".", "_")


def replace_dot_with_underscore(data: dict) -> dict:
    """
    Processes and replace (.)dot present inside keys with (_)underscore.
    Args:
        data (dict): dictionary data
    Returns:
        dict: dictionary with replaced keys
    """
    processed = {}
    for key, value in data.items():
        if isinstance(value, dict):
            value = replace_dot_with_underscore(value)
        if isinstance(value, list):
            temp = []
            for item in value:
                if isinstance(item, dict):
                    item = replace_dot_with_underscore(item)
                temp.append(item)
            value = temp
        processed[key_replace(key)] = value
    return processed


def epoch_to_date(result_data: dict) -> dict:
    """
    Convert dates to valid format.
    Args:
        result_data (dict): dictionary data
    Returns:
        dict: dictionary with modified dates.
    """
    key_list = [
        "last_analysis_date",
        "first_submission_date",
        "last_modification_date",
        "last_https_certificate_date",
    ]
    if result_data.get("data"):
        for key in key_list:
            field_val = result_data.get("data", [])[0].\
                get("attributes", {}).get(key)
            if field_val:
                try:
                    value = parse(field_val).strftime(TIME_FORMAT)
                    result_data["data"][0]["attributes"][key] = value
                except ValueError:
                    return {
                        "error_message": f"This particular "
                                         f"({key}) is not available"
                    }
                except AttributeError:
                    result_data["data"][0]["attributes"][key] = "N/A"
                except KeyError:
                    result_data["data"][0]["attributes"][key] = "N/A"
    else:
        return {"error_message": "no data result found"}
    val = (
        result_data.get("data", [{}])[0]
        .get("attributes", {})
        .get("last_analysis_stats", {})
        .get("suspicious")
    )
    if val is not None:
        result_data.update({"suspiciousRate": val})
    return result_data


def get_formatted_data(result_data: dict) -> dict:
    """
    Converted whois data into valid json format.
    Args:
        result_data (dict): dictionary data
    Returns:
        dict: dictionary with modified dates.
    """
    try:
        if result_data.get("data", [])[0].get("attributes", {}).\
                get("whois"):
            for key, value in result_data.items():
                if key == "data":
                    for item in value:
                        item["attributes"]["whois"] = item["attributes"][
                            "whois"
                        ].replace("\n", "<br/>")
        temp = (
            result_data.get("data", [])[0]
            .get("attributes", {})
            .get("last_analysis_stats")
        )
        status = {
            key: value for key, value in temp.items()
            if value == max(temp.values())
        }
        result_data["enrichment_status"] = status
    except IndexError:
        return {"error_message": "no results found"}
    return result_data


def process_and_modify_comment_data(response_data: dict) -> dict:
    """
    Converted attribute whois to list containing string information.
    Args:
        response_data (dict): dictionary data
    Returns:
        dict: dictionary with modified dates.
    """
    try:
        whois_string = response_data.get("data", {}).\
            get("attributes", {}).get("whois")

        if whois_string is None:
            response_data["data"]["attributes"]["whois"] = \
                ["No comments found."]

        elif whois_string:
            whois_string = whois_string.replace("\n", "").\
                replace("\r", "").strip()
            whois_string = whois_string.split("Comment:")[:-1]
            response_data["data"]["attributes"]["whois"] = whois_string

        return response_data

    except KeyError as err:
        return {"error_message": f"No comments found. "
                                 f"Error while processing request: "
                                 f"{str(err)}"}


def process_and_modify_epoch_to_date_format(response_data: dict) -> dict:
    """
    Converted attribute epoch to human understand date.
    Args:
        response_data (dict): dictionary data
    Returns:
        dict: dictionary with modified dates.
    """
    key_list = ["last_modification_date"]
    for key in key_list:
        try:

            date_field_value = response_data.get("data", {}).\
                get("attributes", {}).get(key)

            response_data["data"]["attributes"][key] = \
                parse(date_field_value).strftime(TIME_FORMAT)
        except ValueError:
            response_data["data"]["attributes"][key] = "N/A"
        except AttributeError:
            response_data["data"]["attributes"][key] = "N/A"
        except KeyError as err:
            return {"error_message": f"No Date parameter found. "
                                     f"Error while processing request: "
                                     f"{str(err)}"}
    return response_data


def process_relationships_data(relationships: str) -> dict:
    """
    Convert relationships string data to dict information.
    Args:
        relationships (str): relationships
    Returns:
        dict: dictionary with modified data.
    """
    list_of_valid_relationships = []
    list_of_relationships = relationships.split(",")
    list_of_relationships = [
        relationship.strip() for relationship in list_of_relationships
    ]
    try:
        for relationship in list_of_relationships:
            list_of_valid_relationships.append(
                generic_functions.loads_enum(
                    relationship, RelationshipEnum
                ).name.lower()
            )
        relationships = ",".join(list_of_valid_relationships)
        return {"relationships_data": relationships}
    except AttributeError as e:
        relationships_list = []
        for relationship in RelationshipEnum:
            relationships_list.append(relationship.name.lower())
        error_message = (
            f'"{str(e).lower()}" is not one of '
            f'allowed categories {relationships_list}'
            f"<br> Please provide allowed category or check for typos"
        )
        return {"error_message": error_message}


def create_relationship_data_when_relationships_is_empty() -> str:
    """
    Create relationships data.
    Returns:
        dict: dictionary with modified data.
    """
    list_of_valid_relationships = []
    for relationship in RelationshipEnum:
        list_of_valid_relationships.append(relationship.name.lower())
    relationships = ",".join(list_of_valid_relationships)
    return relationships
