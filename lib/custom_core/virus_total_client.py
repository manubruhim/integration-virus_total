from client_tools import RestClient


class VirusTotalClient(RestClient):
    """
    Provides an web API client for VirusTotal.
    Arguments:
        base_url (str): base url of VirusTotal.
        api_key (str) :The secret key for the authentication.
    """

    def __init__(self, base_url: str, api_key: str, **kwargs):
        super().__init__(base_url, headers={"X-Apikey": api_key}, **kwargs)

    def enrich(self, query: str) -> dict:
        """
        This method will use to execute search action operation.
        Arguments:
            query:

        Returns:
            To execute rest api call.

        """
        return self.get("/search", params={"query": query})

    def get_investigation_enrichment(
        self, collection_name: str, object_id: str, relationships: str
    ) -> dict:
        """
        This method will use to execute action for ip investigation enrichment.
        Arguments:
            collection_name: This will be remain same as ip_address
            object_id: It will be the ip address value
            relationships: Comma separated relationships value.

        Returns:
            To execute rest api call.

        """
        return self.get(
            f"/{collection_name}/{object_id}",
            params={"relationships": relationships}
        )
