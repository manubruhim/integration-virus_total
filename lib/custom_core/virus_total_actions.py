from custom_core.virus_total_client import VirusTotalClient
from st2_utils import STActions


class VirusTotalActions(STActions):
    CLIENT_OBJECT = VirusTotalClient
    CONFIG_KEYS = ("api_key",)
    OPTIONAL_CONFIG_KEYS = (("base_url", "https://www.virustotal.com/api/v3"),
                            ("connect_timeout", 60), ("read_timeout", 60))
