"""
Generated with common logic v2.0.5
"""

from st2_utils import ActionsWrapper
from st2common.runners.base_action import Action

from enrich_actions import EnrichActions


class EnrichUrl(Action):
    """
    StackStorm generated action that provides an entry point to Python implementation.
    """

    @ActionsWrapper()
    def run(self, instance_name: str, url: str) -> dict:
        """
        Get information from VirusTotal about a certain url.

        Args:
            instance_name(str): The key name of the configuration.
            url(str): A single url to look up if it is a threat.

        Returns:
            dict: Representing url findings.
        """

        return_value = EnrichActions(logger=self.logger).enrich_url(
            instance_name=instance_name, url=url
        )
        return return_value
