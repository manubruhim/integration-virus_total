"""
Generated with common logic v2.0.5
"""

from st2_utils import ActionsWrapper
from st2common.runners.base_action import Action

from enrich_actions import EnrichActions


class GetIpAddressInvestigationEnrichment(Action):
    """
    StackStorm generated action that provides an entry point to Python implementation.
    """

    @ActionsWrapper()
    def run(self, instance_name: str, ip: str, relationships: str = None) -> dict:
        """
        Generic action based functions.
            Arguments:
                instance_name(str): The key name of the configurations.
                ip(str): A single ip against which various relationship details will get pull.
                relationships(:obj: str  optional): comma separated relationship value details.

            Returns:
                dict: Representing action finding.
        """

        return_value = EnrichActions(
            logger=self.logger
        ).get_ip_address_investigation_enrichment(
            instance_name=instance_name, ip=ip, relationships=relationships
        )
        return return_value
