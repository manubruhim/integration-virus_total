if __name__ == "__main__":
    # Maps all the actions and generates them.
    from st2_utils import STActions
    from enrich_actions import EnrichActions

    # add here imports for all relevant action classes

    STActions.generate()
    STActions.generate_readme()
    STActions.generate_workflows()
