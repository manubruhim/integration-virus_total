"""
Generated with common logic v2.0.5
"""

from st2_utils import ActionsWrapper
from st2common.runners.base_action import Action

from enrich_actions import EnrichActions


class EnrichFileHash(Action):
    """
    StackStorm generated action that provides an entry point to Python implementation.
    """

    @ActionsWrapper()
    def run(self, instance_name: str, file_hash: str) -> dict:
        """
        Get information from VirusTotal about a certain file hash.

        Args:
            instance_name(str): The key name of the configuration.
            file_hash(str): A single file hash to look up if it is a threat (MD5, SHA1, SHA256).

        Returns:
            dict: Representing file hash findings.
        """

        return_value = EnrichActions(logger=self.logger).enrich_file_hash(
            instance_name=instance_name, file_hash=file_hash
        )
        return return_value
