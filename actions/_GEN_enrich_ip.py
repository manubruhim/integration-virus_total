"""
Generated with common logic v2.0.5
"""

from st2_utils import ActionsWrapper
from st2common.runners.base_action import Action

from enrich_actions import EnrichActions


class EnrichIp(Action):
    """
    StackStorm generated action that provides an entry point to Python implementation.
    """

    @ActionsWrapper()
    def run(self, instance_name: str, ip: str) -> dict:
        """
        Get information from VirusTotal about a certain ip.

        Args:
            instance_name(str): The key name of the configuration.
            ip(str): A single ip to look up if it is a threat.

        Returns:
            dict: Representing ip findings.
        """

        return_value = EnrichActions(logger=self.logger).enrich_ip(
            instance_name=instance_name, ip=ip
        )
        return return_value
