import validators

from custom_core.virus_total_actions import VirusTotalActions
from custom_core.utils import replace_dot_with_underscore
from custom_core.utils import epoch_to_date
from custom_core.utils import get_formatted_data
from custom_core.utils import process_and_modify_comment_data
from custom_core.utils import process_relationships_data
from custom_core.utils import process_and_modify_epoch_to_date_format
from custom_core.utils import create_relationship_data_when_relationships_is_empty
from actions_utils import RegisterAction, RolesBase
from st2_utils import Tags
from client_tools import APIError
from general.validators import is_ip_address


class EnrichActions(VirusTotalActions):
    @RegisterAction(
        role=RolesBase.READ,
        tags=(
            (Tags.PRINTABLE, "virus_total_ip_enrichment"),
            (Tags.CLIABLE, "virus_total_ip_enrichment"),
        ),
    )
    def enrich_ip(self, instance_name: str, ip: str) -> dict:
        """
        Get information from VirusTotal about a certain ip.

        Args:
            instance_name(str): The key name of the configuration.
            ip(str): A single ip to look up if it is a threat.

        Returns:
            dict: Representing ip findings.

        """

        if not validators.ip_address.ipv4(ip):
            return {"error_message": "Given IP is not valid"}

        return self.get_data(instance_name, ip)

    @RegisterAction(
        role=RolesBase.READ,
        tags=(
            (Tags.PRINTABLE, "virus_total_url_enrichment"),
            (Tags.CLIABLE, "virus_total_url_enrichment"),
        ),
    )
    def enrich_url(self, instance_name: str, url: str) -> dict:
        """
        Get information from VirusTotal about a certain url.

        Args:
            instance_name(str): The key name of the configuration.
            url(str): A single url to look up if it is a threat.

        Returns:
            dict: Representing url findings.

        """

        if not validators.url(url):
            return {"error_message": "Given URL is not valid"}

        return self.get_data(instance_name, url)

    @RegisterAction(
        role=RolesBase.READ,
        tags=(
            (Tags.PRINTABLE, "virus_total_domain_enrichment"),
            (Tags.CLIABLE, "virus_total_domain_enrichment"),
        ),
    )
    def enrich_domain(self, instance_name: str, domain: str) -> dict:
        """
        Get information from VirusTotal about a certain domain.

        Args:
            instance_name(str): The key name of the configuration.
            domain(str): A single domain to look up if it is a threat.

        Returns:
            dict: Representing domain findings.

        """

        if not validators.domain(domain):
            return {"error_message": "Given Domain is not valid"}

        return self.get_data(instance_name, domain)

    @RegisterAction(
        role=RolesBase.READ,
        tags=(
            (Tags.PRINTABLE, "virus_total_filehash_enrichment"),
            (Tags.CLIABLE, "virus_total_filehash_enrichment"),
        ),
    )
    def enrich_file_hash(self, instance_name: str, file_hash: str) -> dict:
        """
        Get information from VirusTotal about a certain file hash.

        Args:
            instance_name(str): The key name of the configuration.
            file_hash(str): A single file hash to look up if it is a threat (MD5, SHA1, SHA256).

        Returns:
            dict: Representing file hash findings.

        """

        if (
            validators.hashes.md5(file_hash)
            or validators.hashes.sha1(file_hash)
            or validators.hashes.sha256(file_hash)
        ):
            return self.get_data(instance_name, file_hash)
        else:
            return {"error_message": "Given File Hash is not valid"}

    def get_data(self, instance_name: str, action: str) -> dict:
        """
        Generic action based function.
            Args:
                instance_name(str): The key name of the configuration.
                action(str): desired enrich action.

            Returns:
                dict: Representing action findings.

        """

        with self.get_client(instance_name) as client:
            try:
                data = client.enrich(action)
            except APIError as api_err:
                return {"error_message": f"{api_err}"}
            else:
                result_data = replace_dot_with_underscore(data)
                formated_data = get_formatted_data(result_data)
                result_data_modified = epoch_to_date(formated_data)
            return result_data_modified

    @RegisterAction(
        role=RolesBase.READ,
    )
    def get_ip_address_investigation_enrichment(
        self, instance_name: str, ip: str, relationships: str = None
    ) -> dict:

        """
        Generic action based functions.
            Arguments:
                instance_name(str): The key name of the configurations.
                ip(str): A single ip against which various relationship details will get pull.
                relationships(:obj: str  optional): comma separated relationship value details.

            Returns:
                dict: Representing action finding.
        """

        if "/" in ip:
            return {
                "error_message": "Seemo has detected that you have"
                                 " entered CIDR, please enter a Single"
                                 " IP to be enriched."
            }

        if not is_ip_address(ip):
            return {"error_message": "Given IP is not valid"}

        if not relationships or relationships.isspace():
            relationships = create_relationship_data_when_relationships_is_empty()

        else:
            relationships_data = process_relationships_data(relationships)

            if "error_message" in relationships_data:
                return relationships_data

            else:
                relationships = relationships_data["relationships_data"]

        with self.get_client(instance_name) as client:
            resp = client.get_investigation_enrichment(
                collection_name="ip_addresses",
                object_id=ip,
                relationships=relationships,
            )
        modified_data = process_and_modify_comment_data(resp)

        if "error_message" in modified_data:
            return modified_data

        formatted_output_data = process_and_modify_epoch_to_date_format(modified_data)

        return formatted_output_data
