"""
Generated with common logic v2.0.5
"""

from st2_utils import ActionsWrapper
from st2common.runners.base_action import Action

from enrich_actions import EnrichActions


class EnrichDomain(Action):
    """
    StackStorm generated action that provides an entry point to Python implementation.
    """

    @ActionsWrapper()
    def run(self, instance_name: str, domain: str) -> dict:
        """
        Get information from VirusTotal about a certain domain.

        Args:
            instance_name(str): The key name of the configuration.
            domain(str): A single domain to look up if it is a threat.

        Returns:
            dict: Representing domain findings.
        """

        return_value = EnrichActions(logger=self.logger).enrich_domain(
            instance_name=instance_name, domain=domain
        )
        return return_value
