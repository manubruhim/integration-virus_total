image: python:3.6.9

definitions:
  services:
    mongo:
      image: mongo:3.4
      environment:
        MONGODB_USER: mongodb
        MONGODB_PASS: mongodb
        MONGODB_DATABASE: cproof
        MONGO_DATA_DIR: /data/db
        command: mongod --smallfiles
    rabbitmq:
      image: rabbitmq:3
      # environment:
      #  RABBITMQ_DEFAULT_USER: rabbitmq
      #  RABBITMQ_DEFAULT_PASS: rabbitmq
  steps:
    - step: &platform-awake
        name: Platform awake
        script:
          - export PACK_NAME=${BITBUCKET_REPO_SLUG/integration-/}
          - export NAMESPACE_NAME="uca-${PACK_NAME//_/-}"
          - pipe: atlassian/trigger-pipeline:4.1.5
            variables:
              BITBUCKET_USERNAME: "${BITBUCKET_APP_USERNAME}"
              BITBUCKET_APP_PASSWORD: "${BITBUCKET_APP_PASSWORD}"
              ACCOUNT: "bisec"
              REPOSITORY: "delivery-for-testing"
              BRANCH_NAME: 'master'
              CUSTOM_PIPELINE_NAME: 'platform-awake'
              PIPELINE_VARIABLES: >
                [{
                  "key": "NAME",
                  "value": "${NAMESPACE_NAME}"
                }]
              WAIT: 'true'
    - step: &download-dependencies
        name: Download dependencies
        caches:
          - pip
        script:
          - export VIRTUALENV_DIR="~/virtualenv"
          - export ST2_INSTALL_DEPS="0"
          - (umask 077 ; echo $CI_BITBUCKET_SSH_PRIVATE | base64 --decode > ~/.ssh/id_rsa)
          - git clone -b master git@bitbucket.org:bisec/st2-ci.git ~/ci
          - ~/ci/.circle/dependencies ; ~/ci/.circle/exit_on_py3_checks $?
          - mv ~/ci ci
          - mv ~/virtualenv virtualenv
          - mkdir tmp
          - mv /tmp/st2 tmp/st2
        artifacts:
          - ci/**
          - virtualenv/**
          - tmp/st2/**
          - Makefile
    - step: &run-tests-python36
        name: Run unit tests (Python 3.6)
        caches:
          - pip
        script:
          - mv ci ~/ci
          - mv virtualenv ~/virtualenv
          - mv tmp/st2 /tmp/st2
          - export VIRTUALENV_DIR="~/virtualenv"
          - export ST2_INSTALL_DEPS="0"
          - export FORCE_CHECK_ALL_FILES="true"
          - ~/ci/.circle/test ; ~/ci/.circle/exit_on_py3_checks $?
          - mv ~/ci ci
          - mv ~/virtualenv virtualenv
          - mv /tmp/st2 tmp/st2
        artifacts:
          - ci/**
          - virtualenv/**
          - tmp/st2/**
        services:
          - mongo
          - rabbitmq
    - step: &deploy
        name: Deploy
        caches:
          - pip
        script:
          - export VIRTUALENV_DIR="~/virtualenv"
          - mv ci ~/ci
          - mv virtualenv ~/virtualenv
          - mv tmp/st2 /tmp/st2
          - rm Makefile
          - apt update -y
          - apt install -y gmic optipng
          - ~/ci/.circle/deployment ; ~/ci/.circle/exit_on_py3_checks $?
        services:
          - mongo
          - rabbitmq
    - step: &prepare_deploy_scripts
        name: Prepare pipeline deploy scripts
        script:
          - (umask  077 ; echo $CI_SSH_PRIVATE | base64 -d > ~/.ssh/id_rsa)
          - export PACK_NAME=${BITBUCKET_REPO_SLUG/integration-/}
          - mkdir -p /tmp/$PACK_NAME
          - cp -R ./functional_tests/* /tmp/$PACK_NAME
          - git clone --branch ${AUTOMATION_INFRA_BRANCH:=master} ssh://git@bitbucket.org/bisec/stackstorm_test_cases.git .pipeline
          - mkdir -p .pipeline/tests/stackstorm/$PACK_NAME
          - cp -R /tmp/$PACK_NAME/* .pipeline/tests/stackstorm/$PACK_NAME
          - ls -alR .pipeline/tests/stackstorm/$PACK_NAME
        artifacts:
          - .pipeline/**
          - .env
    - step: &fetch_deployment_details
        name: Fetch deployment details
        image:
          name: artifactory.dagility.com/cyberproof-docker/k8s-az-cli:latest
          username: $JFROG_DOCKER_USERNAME
          password: $JFROG_DOCKER_PASSWORD
        script:
          - bash ./.pipeline/.deployment/scripts/prepare-env.sh
          - bash ./.pipeline/.deployment/scripts/fetch-deployment.sh
          - bash ./.pipeline/.deployment/scripts/az-login.sh
          - bash ./.pipeline/.deployment/scripts/az-fetch-secrets.sh
        artifacts:
          - .env
          - "st2.connection.json"
          - .kubeconfig
    - step: &run_functional_tests
        image:
          name: bisec/cdc_testcases-runner:latest
          username: $DOCKER_HUB_USERNAME
          password: $DOCKER_HUB_PASSWORD
        size: 2x
        name: Run functional tests
        script:
          - bash ./.pipeline/.deployment/scripts/run-single-test.sh
        caches:
          - docker
        services:
          - docker
        artifacts:
          - ".env"
          - ".pipeline/results/*"
        after-script:
          - bash ./.pipeline/.deployment/scripts/generate-allure-report.sh
          - bash ./.pipeline/.deployment/scripts/slack-notify.sh
          - bash ./.pipeline/.deployment/scripts/cleanup.sh
    - step: &cleanup
        name: Cleanup
        image:
          name: artifactory.dagility.com/cyberproof-docker/k8s-az-cli:latest
          username: $JFROG_DOCKER_USERNAME
          password: $JFROG_DOCKER_PASSWORD
        script:
          - bash ./.pipeline/.deployment/scripts/cleanup.sh
pipelines:
  default:
    - step: *download-dependencies
    - step: *run-tests-python36
  branches:
    master:
      - step: *platform-awake
      - parallel:
          - step: *download-dependencies
          - step: *prepare_deploy_scripts
      - step: *fetch_deployment_details
      - step: *run_functional_tests
      - step: *deploy
      - step: *cleanup
  pull-requests:
    "*":
      - step: *platform-awake
      - parallel:
          - step: *download-dependencies
          - step: *prepare_deploy_scripts
      - step: *fetch_deployment_details
      - step: *run_functional_tests
      - step: *cleanup
  custom:
    run-tests-specific-env:
      - variables:
          - name: ENV
      - parallel:
          - step: *download-dependencies
          - step: *prepare_deploy_scripts
      - step: *fetch_deployment_details
      - step: *run_functional_tests
      - step: *cleanup
    run-tests-based-specific-infra:
      - variables:
          - name: ENV
          - name: AUTOMATION_INFRA_BRANCH
      - parallel:
          - step: *download-dependencies
          - step: *prepare_deploy_scripts
      - step: *fetch_deployment_details
      - step: *run_functional_tests
      - step: *cleanup